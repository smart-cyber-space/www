## Smart Cyber Space (SCS) Group at The University of North Texas

Welcome to Smart Cyber Space (SCS) Group at the Department of Computer Science and Engineering at The University of North Texas. The main research interests include but not limited to: 

* Cybersecurity
* Cloud Computing 
* Internet of Things (IoT)
* Unmanned Vehicles (UVs)
* Social media analysis for cyber-threats 
